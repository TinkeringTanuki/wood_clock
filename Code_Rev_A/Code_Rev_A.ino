 /*
 * 
 * Devin Cooper
 * Wood alarm clock with NEOpixel matrixs
 * Rev_a 8/5/18
 * Based on:
Allarm_Clock By: Tiziano Bianchettin https://www.hackster.io/Tittiamo/alarm-clock-f61bad



 
*/
//************libraries**************//
#include <Wire.h>
#include <RTClib.h>
//#include "DS3231.h"
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

//************************************//
#ifndef PSTR
 #define PSTR // Make Arduino Due happy
#endif
#define MATRIX 5

//RTClib RTC;
RTC_DS1307 RTC;


//************Button*****************//
int P1=6; // Button SET MENU'
int P2=7; // Button +
int P3=8; // Button -
int P4=9; // SWITCH Alarm

//**************Alarm***************//
#define LED 13
#define buzzer 4

//************Variables**************//
int hourupg;
int minupg;
int yearupg;
int monthupg;
int dayupg;
int menu =0;
int format =0;
int setAll =0;

uint8_t alarmHours = 10, alarmMinutes = 25;  // Holds the current alarm time

// MATRIX DECLARATION:
// Parameter 1 = width of NeoPixel matrix
// Parameter 2 = height of matrix
// Parameter 3 = pin number (most are valid)
// Parameter 4 = matrix layout flags, add together as needed:
//   NEO_MATRIX_TOP, NEO_MATRIX_BOTTOM, NEO_MATRIX_LEFT, NEO_MATRIX_RIGHT:
//     Position of the FIRST LED in the matrix; pick two, e.g.
//     NEO_MATRIX_TOP + NEO_MATRIX_LEFT for the top-left corner.
//   NEO_MATRIX_ROWS, NEO_MATRIX_COLUMNS: LEDs are arranged in horizontal
//     rows or in vertical columns, respectively; pick one or the other.
//   NEO_MATRIX_PROGRESSIVE, NEO_MATRIX_ZIGZAG: all rows/columns proceed
//     in the same order, or alternate lines reverse direction; pick one.
//   See example below for these values in action.
// Parameter 5 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)



//Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, MATRIX,  NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800); //Quimat from amazon
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, MATRIX,  NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800); //BTF-LIGHTING from amazon

const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };


// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
   if(WheelPos < 85) {
   return matrix.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
   return matrix.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else if (WheelPos < 245) {
   WheelPos -= 170;
   return matrix.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
   else {
    return matrix.Color(175,255,255);
  }
}
//int x    = matrix.width();
//int pass = 0;
byte color = 0;
byte bright = 5;

void setup()
{


  matrix.begin();
  matrix.setTextWrap(true);
  matrix.setBrightness(5);
  //matrix.setTextColor(Wheel(color));
  matrix.setTextColor(Wheel(color));
 // matrix.setTextColor(matrix.Color(175,255,255)); 
  //matrix.setTextColor(matrix.Color(110,200,255));
  matrix.setRotation(2);
  

  pinMode(P1,INPUT_PULLUP); // https://www.arduino.cc/en/Tutorial/InputPullupSerial
  pinMode(P2,INPUT_PULLUP);
  pinMode(P3,INPUT_PULLUP);
  pinMode(P4,INPUT_PULLUP);
  pinMode(LED,OUTPUT);
  pinMode(buzzer, OUTPUT); // Set buzzer as an output
  printAllOff();
 // Serial.begin(9600);
  Wire.begin();
  RTC.begin();

  if (! RTC.isrunning()) {
   // Serial.println("RTC is NOT running!");
    // Set the date and time at compile time
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  // RTC.adjust(DateTime(__DATE__, __TIME__)); //removing "//" to adjust the time
    // The default display shows the date and time
  int menu=0;
}


void loop()
{

  /*
  matrix.fillScreen(0);
  matrix.setCursor(x, 0);
  matrix.print(F("BUILD A. SQUID. make your own squid and set. it free. BUILD YOUR. SQUID. FIND YOUR. SQUID { MMMMMMMMMMMMMMMMMM } YOUR SQUID"));
  if(--x < -750) {
    x = matrix.width();
    if(++pass >= 3) pass = 0;
    matrix.setTextColor(colors[pass]);
  }
  matrix.show();
  delay(100);

*/

// check if you press the SET button and increase the menu index
  if(digitalRead(P1)== LOW) 
  {
   menu=menu+1;
   
  }
  if(digitalRead(P2)== LOW)
  {
    if (menu==0)
    {    
  color=color+5;
  if(color >= 255) (color = 0); //, matrix.setTextColor(matrix.Color(175,255,255)); 
  matrix.setTextColor(Wheel(color));
    }
  }
  if(digitalRead(P3)== LOW)
  {
    if (menu==0)
    {
    if(++bright >= 255) bright = 5;
    bright=bright+5;
   matrix.setBrightness(bright);
    }
  }
  if((digitalRead(P2)== LOW)&&(digitalRead(P3)== LOW))                                                                                                                                                                     
  {
    
    DisplaySetHourAll();
    DisplaySetMinuteAll();
    matrix.clear();
    matrix.setCursor(0,0);
    matrix.print("ALARM");
     matrix.show();
     delay(1000);
     matrix.clear();
    matrix.setCursor(0,0);
    matrix.print(alarmHours, DEC);
    matrix.print(":");
    matrix.print(alarmMinutes, DEC);
    matrix.show();
    delay(2000);
    matrix.clear();
  }
// in which subroutine should we go?
  if (menu==0)
    {
     DisplayDateTime(); // void DisplayDateTime
     Alarm(); // Alarm control
     matrix.clear();
          }
  if (menu==1)
    {
    DisplaySetHour();
    }
  if (menu==2)
    {
    DisplaySetMinute();
    }
  if (menu==3)
    {
      DisplaySetFormat();
    }
  /*
   
   if (menu==3)
    {
    DisplaySetYear();
    }
  if (menu==4)
    {
    DisplaySetMonth();
    }
  if (menu==5)
    {
    DisplaySetDay();
    }
    */
  if (menu==4)
    {
    StoreAgg(); 
    delay(500);
    menu=0;
    }
    delay(100);
}

void DisplayDateTime ()
{
// We show the current date and time
  DateTime now = RTC.now();
   // matrix.clear(); added this command elsewhere
  //  matrix.fillScreen(matrix.Color(175,0,0));
  matrix.setCursor(1,1);
 //matrix.print("Hour : ");
  
  if (format==0)
  {
    if (now.hour()<=9)
  {
    matrix.print("0");
  }
  matrix.print(now.hour(), DEC);
  hourupg=now.hour();
  }
    if (format==1)
  {
    if (now.hour()<=9) matrix.setCursor(5,0);
    if (now.hour() >12)   matrix.print((now.hour()-12), DEC);
    if (now.hour() <=12) matrix.print(now.hour(), DEC);
  hourupg=now.hour();
  }
  matrix.print(":");
  if (now.minute()<=9)
  {
    matrix.print("0");
  }
  matrix.print(now.minute(), DEC);
  minupg=now.minute();
  matrix.show();

  /*
  matrix.print(":");
  if (now.second()<=9)
  {
    matrix.print("0");
  }
  matrix.print(now.second(), DEC);
  matrix.setCursor(0, 2);
  matrix.print("Date : ");
  if (now.day()<=9)
  {
    matrix.print("0");
  }
  matrix.print(now.day(), DEC);
  dayupg=now.day();
  matrix.print("/");
  if (now.month()<=9)
  {
    matrix.print("0");
  }
  matrix.print(now.month(), DEC);
  monthupg=now.month();
  matrix.print("/");
  matrix.print(now.year(), DEC);
  yearupg=now.year();
 
  char DOW[][10]={"Sunday   ","Monday   ","Tuesday  ","Wednesday","Thursday ","Friday   ","Saturday "};
  matrix.setCursor(0, 0); 
  matrix.print("Day  : ");
  matrix.print(DOW[now.dayOfTheWeek()]); // if it appears error in the code, enter the code given below
  //matrix.print(DOW[now.dayOfWeek()]);
  */
}

void DisplaySetHour()
{
// time setting
  matrix.clear();
  DateTime now = RTC.now();
  if(digitalRead(P2)==LOW)
  {
    if(hourupg==23)
    {
      hourupg=0;
    }
    else
    {
      hourupg=hourupg+1;
    }
  }
   if(digitalRead(P3)==LOW)
  {
    if(hourupg==0)
    {
      hourupg=23;
    }
    else
    {
      hourupg=hourupg-1;
    }
  }
  matrix.setCursor(0,0);
  matrix.print("HR");
  // matrix.show();
 // delay(500);
  //  matrix.clear();
  matrix.setCursor(20,0);
  matrix.print(hourupg,DEC);
  matrix.show();

  delay(200);
}

void DisplaySetMinute()
{
// Setting the minutes
  matrix.clear();
  if(digitalRead(P2)==LOW)
  {
    if (minupg==59)
    {
      minupg=0;
    }
    else
    {
      minupg=minupg+1;
    }
  }
   if(digitalRead(P3)==LOW)
  {
    if (minupg==0)
    {
      minupg=59;
    }
    else
    {
      minupg=minupg-1;
    }
  }
  matrix.setCursor(0,0);
  matrix.print("Min ");
   // matrix.show();
   // delay(500);
      //matrix.clear();
  matrix.setCursor(20,0);
  matrix.print(minupg,DEC);
  matrix.show();
  delay(250);
}

void DisplaySetFormat()
{
    matrix.clear();
    matrix.setCursor(12,0);
    if(digitalRead(P2)==LOW)
  {
    if (format==1)
    {
      minupg=0;
    }
    else
    {
      format=format+1;
    }
  }
   if(digitalRead(P3)==LOW)
  {
    if (format==0)
    {
      format=1;
    }
    else
    {
      format=format-1;
    }
  }
if (format==0) matrix.print("24HR");
if (format==1) matrix.print("12HR");
matrix.show();
delay(250);
}
  
void DisplaySetYear()
{
// setting the year
  matrix.clear();
  if(digitalRead(P2)==LOW)
  {    
    yearupg=yearupg+1;
  }
   if(digitalRead(P3)==LOW)
  {
    yearupg=yearupg-1;
  }
  matrix.setCursor(0,0);
  matrix.print("Set Year:");
  matrix.setCursor(0,1);
  matrix.print(yearupg,DEC);
  delay(200);
}

void DisplaySetMonth()
{
// Setting the month
  matrix.clear();
  if(digitalRead(P2)==LOW)
  {
    if (monthupg==12)
    {
      monthupg=1;
    }
    else
    {
      monthupg=monthupg+1;
    }
  }
   if(digitalRead(P3)==LOW)
  {
    if (monthupg==1)
    {
      monthupg=12;
    }
    else
    {
      monthupg=monthupg-1;
    }
  }
  matrix.setCursor(0,0);
  matrix.print("Set Month:");
  matrix.setCursor(0,1);
  matrix.print(monthupg,DEC);
  delay(200);
}

void DisplaySetDay()
{
// Setting the day
  matrix.clear();
  if(digitalRead(P2)==LOW)
  {
    if (dayupg==31)
    {
      dayupg=1;
    }
    else
    {
      dayupg=dayupg+1;
    }
  }
   if(digitalRead(P3)==LOW)
  {
    if (dayupg==1)
    {
      dayupg=31;
    }
    else
    {
      dayupg=dayupg-1;
    }
  }
  matrix.setCursor(0,0);
  matrix.print("Set Day:");
  matrix.setCursor(0,1);
  matrix.print(dayupg,DEC);
  delay(200);
}

void StoreAgg()
{
// Variable saving
  matrix.clear();
  matrix.setCursor(0,0);
  matrix.print("SAVED");
  matrix.show();
  RTC.adjust(DateTime(yearupg,monthupg,dayupg,hourupg,minupg,0));
  delay(1000);
}
void DisplaySetHourAll()// Setting the alarm minutes
{
  while(digitalRead(P1)==HIGH){

  matrix.clear();

  if(digitalRead(P2)==LOW)
  {
    if(alarmHours==23)
    {
      alarmHours=0;
    }
    else
    {
      alarmHours=alarmHours+1;
    }
  }
   if(digitalRead(P3)==LOW)
  {
    if(alarmHours==0)
    {
      alarmHours=23;
    }
    else
    {
      alarmHours=alarmHours-1;
    }
  }
  matrix.clear();
  matrix.setCursor(0,0);
  matrix.print("AH");
  matrix.setCursor(15,0);
  matrix.print(alarmHours,DEC);
  matrix.show();
  delay(200);
 }
 delay(200);
}

void DisplaySetMinuteAll()// Setting the alarm minutes
 {
  while(digitalRead(P1)==HIGH){ 

  matrix.clear();
  if(digitalRead(P2)==LOW)
  {
    if (alarmMinutes==59)
    {
      alarmMinutes=0;
    }
    else
    {
      alarmMinutes=alarmMinutes+1;
    }
  }
   if(digitalRead(P3)==LOW)
  {
    if (alarmMinutes==0)
    {
      alarmMinutes=59;
    }
    else
    {
      alarmMinutes=alarmMinutes-1;
    }
  }
  matrix.clear();
  matrix.setCursor(0,0);
  matrix.print("AM");
  matrix.setCursor(15,0);
  matrix.print(alarmMinutes,DEC);
  matrix.show();
  delay(200);
 }
 delay(200);
}
void printAllOn(){
  matrix.drawPixel(31,7, Wheel((color)));
  /*
   delay(500);
  matrix.clear();
   matrix.setCursor(0,0);
  matrix.print("Alarm: ");
  matrix.show();
delay(500);
 matrix.clear();
  
  
  if (alarmHours <= 9)
  {
    matrix.print("0");
  }
  matrix.print(alarmHours, DEC);
  
  matrix.print(":");
  if (alarmMinutes <= 9)
  {
    matrix.print("0");
  }
  matrix.print(alarmMinutes, DEC); 
  */
  matrix.show();
  
}

void printAllOff() {
   matrix.clear();
  matrix.setCursor(0,0);
  matrix.print("Al Off");  
  matrix.show();
}
void Alarm(){

   if(digitalRead(P4)== LOW)
  {
   setAll=setAll+1;
  }
  if (setAll==0)
    {
    // printAllOff();
     noTone (buzzer);
     digitalWrite(LED,LOW);
     }
  if (setAll==1)
    {

     printAllOn();    
  
     DateTime now = RTC.now();
     if ( now.hour() == alarmHours && now.minute() == alarmMinutes )
        {
          matrix.setTextColor(matrix.Color(0,0,0)); 
          while(digitalRead(P4)== HIGH){
          static float in = 4.712;
          float out;
         DateTime now = RTC.now();
         matrix.clear();

  in = in + 0.05; // increasing this number speeds up the pulse rate
  if (in > 10.995)
    in = 4.712;
  out = sin(in) * (bright/2) + (bright/2);
  matrix.setBrightness(out);

        matrix.fillScreen(Wheel(color));
         
         DisplayDateTime();




   }
              
        /*       
         tone(buzzer,880); //play the note "A5" (LA5)
         delay (500);
         tone(buzzer,698); //play the note "F6" (FA5)
        */


        }
        }
    else{
         noTone (buzzer);
         digitalWrite(LED,LOW);
         matrix.setBrightness(bright);
         matrix.setTextColor(Wheel(color));
        }
    
    
  if (setAll==2)
    {
     setAll=0;
    }
    delay(200);
    
}
